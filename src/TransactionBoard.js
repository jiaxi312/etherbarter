import * as React from 'react';


export default function Board(props) {
  const data = props.data;
  const trader = props.trader.toLowerCase();
  const currentAccount = props.account.toLowerCase();

  const traderData = data[Object.keys(data).find(k => k.toLowerCase() === trader)];
  const currentData = data[Object.keys(data).find(k => k.toLowerCase() === currentAccount)];

  return (
    <React.Fragment>
      <div>Account: {trader}</div>
      <p></p>
      <div>
        Provided:
        <div>
          {traderData.CollegeNFT.map((elem) => `Name: CollegeNFT NO. ${elem}\n`)}
          {traderData.UTBC.map((elem) => `Name: UTBC NO. ${elem}\n`)}
        </div>
      </div>
      <p></p>
      <div>
        Requested:
        <div>
          {currentData.CollegeNFT.map((elem) => `Name: CollegeNFT NO. ${elem}\n`)}
          {currentData.UTBC.map((elem) => `Name: UTBC NO. ${elem}\n`)}
        </div>
      </div>
    </React.Fragment>
  );
};