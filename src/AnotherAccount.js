import * as React from 'react';
import { useState, useEffect } from "react";
import { ethers } from "ethers";
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import { FormControlLabel, FormGroup, Checkbox } from '@mui/material'
import './styles/Account.css';
import MetaMask from './assets/metamask-fox.svg';
import EthLogo from './assets/eth_logo.svg';
import BarterSystem from './utils/BarterSystem.json';


/**
 * Slices the account
 */
const sliceAccount = (account) => {
  return `${account.slice(0, 5)}...${account.slice(-4)}`
}

const parseCollections = (collections) => {
  const result = [];
  let index = 0;
  while (index < collections.length) {
    if (collections[index] === '') {
      break;
    }
    const name = collections[index++];
    while (index < collections.length) {
      const tokenId = collections[index++];
      if (tokenId === '<EOT>') {
        break;
      }
      result.push({name: name, tokenId: parseInt(tokenId)});
    }
  }
  return result;
}

export default function AnotherAccount(barter_system_address, selectedCallback) {
  const [currentAccount, setCurrentAccount] = useState("");
  const [currentBalance, setCurrentBalance] = useState("");
  const [collections, setCollections] = useState([]);

  /*
   * Connects to Metamask wallet
   */
  const searchAccount = async () => {
    if (currentAccount === "") {
      return;
    }
    try {
      const { ethereum } = window;

      if (!ethereum) {
        alert("Get MetaMask!");
        return;
      }

      console.log('get balance of:', currentAccount);
      const balance = await ethereum.request({
        method: "eth_getBalance",
        params: [
          currentAccount,
          'latest'
        ]
      });
      setCurrentBalance(parseInt(balance, 16) / 1000000000000000000);

      const provider = new ethers.providers.Web3Provider(ethereum);
      const signer = provider.getSigner();
      const systemContract = new ethers.Contract(
        barter_system_address,
        BarterSystem.abi,
        signer
      );
      let allCollections = await systemContract.getAllCollectionsFrom(currentAccount);
      setCollections(parseCollections(allCollections));
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <React.Fragment>
    <Grid container spacing={3}>
    <Grid item xs={12} md={8} lg={9}>
      <Paper
        sx={{
          p: 2,
          display: 'flex',
          flexDirection: 'column',
          height: 240,
        }}
      >
        <React.Fragment>
        <FormGroup style={{maxHeight:'200px', maxWidth: '400px', scrollBehavior: 'smooth'}}>
          {collections.length === 0 ? 'No Collections so far' : 
            collections.map((elem) =>
          <FormControlLabel onChange={(event) => {
            selectedCallback(currentAccount, elem.name, elem.tokenId, event.target.checked);
            }}
            control={<Checkbox />} label={`${elem.name}, tokenId:${elem.tokenId}`}/>)
        }
        </FormGroup>
        </React.Fragment>
      </Paper>
    </Grid>
    <Grid item xs={12} md={4} lg={3}>
    <Paper
        sx={{
          p: 2,
          display: 'flex',
          flexDirection: 'column',
          height: 240,
        }}
      >
      {currentBalance === "" ? 
      <React.Fragment>
        <p></p>
        <TextField id="outlined-basic" label="account" variant="outlined" 
          onChange={(event) => setCurrentAccount(event.target.value)}/>
        <p></p>
        <Button variant="contained" onClick={searchAccount}>Search</Button>
      </React.Fragment>
       :
      <React.Fragment>
      <div className='center'>      
        <img className="photo" src={MetaMask} alt="MetaMask Logo" />
      </div>
      <p/>
      <div className='center'>
        {sliceAccount(currentAccount)}
      </div>
      <div className='center'>
        <img className='photo-small' src={EthLogo}/>
      </div>
      <div className='center'>
        {currentBalance} ETH
      </div>
    </React.Fragment>}
    </Paper>
    </Grid>
  </Grid>
    </React.Fragment>
  );
}