const express = require('express')
const app = express()
const port = 5000

var server = app.listen(8082)
var io = require('socket.io')(server, {
    cors: {
      origin: ["http://localhost:3000","http://localhost:3001"],
      methods: ["GET", "POST"],
      allowedHeaders: ["Access-Control-Allow-Origin","*"],
      credentials: true
    }
  });

// let userSet = new Set();
let active = true;
io.on('connection', (socket) => {
    console.log('socket connected!!!');
    socket.on('message', data => {
        console.log(data);
    })
    socket.on('tokenInfo', data => {
        console.log(data);
        if (active){
            // if (userSet.size >= 2) {
            //     socket.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
            //     // to everyone
            //     socket.broadcast.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
            //     active = false;
            // } else {
                socket.emit('broadcast', "Waiting for the other barterer..");
                socket.broadcast.emit('tokenInfo', data);
            // }
        } else {
            socket.emit('broadcast', "This barter has expired or been dealt !!!");
        }
        console.log("broadcasted");
    })
    socket.on('confirmation', data => {
        console.log(data);
        if (active){
            // if (userSet.size >= 2) {
            //     socket.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
            //     // to everyone
            //     socket.broadcast.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
            //     active = false;
            // } else {
                socket.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
                socket.broadcast.emit('broadcast', "CONGRATULATIONS!\nLOST DEAL STRUCK!");
                active = false;
            // }
        } else {
            socket.emit('broadcast', "This barter has expired or been dealt !!!");
        }
        console.log("broadcasted");
    })
});




app.get('/', (req, res) => res.send('yellow World!!'))
app.listen(port, () => console.log(`example app listening on port ${port}!`))
  
