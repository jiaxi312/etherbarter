import * as React from 'react';
import { useState, useEffect } from "react";
import { ethers } from "ethers";
import { styled, createTheme, ThemeProvider } from '@mui/material/styles';
import CssBaseline from '@mui/material/CssBaseline';
import MuiDrawer from '@mui/material/Drawer';
import Box from '@mui/material/Box';
import MuiAppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import List from '@mui/material/List';
import Typography from '@mui/material/Typography';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Badge from '@mui/material/Badge';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import Paper from '@mui/material/Paper';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import MenuIcon from '@mui/icons-material/Menu';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import NotificationsIcon from '@mui/icons-material/Notifications';
import {mainListItems} from './listItems';
import Account from './Account';
import AnotherAccount from './AnotherAccount';
import Collection from './Collection';
import CollegeNFT from './utils/CollegeNFT.json';
import UTBC from './utils/UTBC.json';
import BarterSystem from './utils/BarterSystem.json';
import JSConfetti from 'js-confetti'
import Board from './TransactionBoard';




function Copyright(props) {
  return (
    <Typography variant="body2" color="text.secondary" align="center" {...props}>
      {'Copyright © '}
      <Link color="inherit" href="https://gitlab.com/jiaxi312/etherbarter">
        EtherBarter
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const drawerWidth = 240;

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== 'open',
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(['width', 'margin'], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, { shouldForwardProp: (prop) => prop !== 'open' })(
  ({ theme, open }) => ({
    '& .MuiDrawer-paper': {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
      boxSizing: 'border-box',
      ...(!open && {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
          easing: theme.transitions.easing.sharp,
          duration: theme.transitions.duration.leavingScreen,
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
          width: theme.spacing(9),
        },
      }),
    },
  }),
);

const mdTheme = createTheme();


const COLLEGENFT_ADDRESS = "0x4bB7BD6e5E213cE8cacAFb3c4473384567496219";
const UTBC_ADDRESS = "0x12b6c0e6cc7a8A70f9F8A40f2FB8D4BfBDaE05f6";
const SYSTEM_ADDRESS = "0xa6D0d204029bb1c6cBa8e822c57a526DA17CaF12";
let CURRENT_ACCOUNT;
let SELECTED_TOKENS;


const { io } = require("socket.io-client");
const socket = io('localhost:8082');
var ioStatus = false;
var dealt = false;

const App = () => {
  const [open, setOpen] = useState(true);
  const [currentAccount, setCurrentAccount] = useState("");
  console.log(currentAccount);
  const toggleDrawer = () => {
    setOpen(!open);
  };

  /*
   * Connects to Metamask wallet
   */
  const connectWallet = async () => {
    try {
      const { ethereum } = window;

      if (!ethereum) {
        alert("Get MetaMask!");
        return;
      }

      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });
      setCurrentAccount(accounts[0]);
      CURRENT_ACCOUNT = accounts[0]
      console.log(currentAccount);
    } catch (error) {
      console.log(error);
    }
  };


  const mintNft = async () => {
    const {ethereum} = window;
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();

    const isCollegeNft = Math.random() < 0.7;
    const tokenName = isCollegeNft ? "CollegeNFT" : "UTBC";
    const address = isCollegeNft ? COLLEGENFT_ADDRESS : UTBC_ADDRESS;
    const abi = isCollegeNft ? CollegeNFT.abi : UTBC.abi;
    
    const connectedContract = new ethers.Contract(
      address,
      abi,
      signer
    );
    let nftTxn = isCollegeNft ? 
                  await connectedContract.mintCollegeNtf(currentAccount) 
                  : await connectedContract.mintUTBC(currentAccount);
    console.log("Mining...please wait", nftTxn);
    await nftTxn.wait();
    alert(`Congradulations, you minted ${tokenName}`);
    console.log(nftTxn);
  }

  const checkNft = async (trader, allTokens) => {

    socket.emit('message', "全体目光向我看齐");


  
    // allTokens = {account: {CollegeNFT:[ids], UTBC:[ids]}}
    // var owned = {}; 
    // var requested = {}; 

    // let tokens = allTokens[currentAccount]
    // owned["CollegeNFT"] = tokens.CollegeNFT;
    // owned["UTBC"] = tokens.UTBC;
  

    // let tokensrequest = allTokens[trader];
    // requested["CollegeNFT"] = tokensrequest.CollegeNFT;
    // requested["UTBC"] = tokensrequest.UTBC;
    
  
  
    var itemInfoDict = allTokens;
    
    // console.log(itemInfoDict);
    socket.emit('tokenInfo', itemInfoDict);
    getmsg();
  }


  const checktemp = async () => {
    let tokenID = 99
    let tokenName = "CollegeNFT"
    var tokenInfo = {
      "tokenName": tokenName,
      "tokenID": tokenID
    };
    var itemInfoDict = {
      "owner" : "0xf39fd6e51aad88f6f4ce6ab8827279cfffb92266",
      "token" : tokenInfo
    }
    // console.log(itemInfoDict);
    socket.emit('tokenInfo', itemInfoDict);
    getmsg();
  }


  const getmsg = async () => {
    // alert("test");
    // Listening to multi event types will leads to repeating bugs.
    if (!ioStatus){
      console.log("test");
      socket.on('broadcast', data => {
        alert(data);
        // check if the trade is done by message checking
        if (data == "CONGRATULATIONS!\nLOST DEAL STRUCK!") {
          setAgreement(true);
          console.log('start transfer');
          transferToken();
          const jsConfetti = new JSConfetti()
          
          jsConfetti.addConfetti({
            emojis: ['💸','💵','✨'],
            emojiSize: 70,
            confettiNumber: 80,
          })
        } 

      });
      socket.on('tokenInfo', data => {
        console.log(data);
        
        // here are data from another side ........
        selectedTokens = data;
        setBoardData(selectedTokens);
        setShowDashBoard(true);
    })
      ioStatus = true;
    }
  }

  const confirmMsg = async () => {
    socket.emit('confirmation', "6666");
  }

  // getmsg();

  const [openSocket, setOpenSocket] = useState(false);

  useEffect(() => {
    if (currentAccount == ""){
      connectWallet()
    }


    if (!openSocket) {
      getmsg();
      setOpenSocket(true);
    }
  });

  let selectedTokens = {};

  const checkAccount = (account) => {
    if (!selectedTokens.hasOwnProperty(account)) {
      selectedTokens[account] = {CollegeNFT: [], UTBC: []};
    }
  }

  const selectCallback = (account, name, tokenId, selected) => {
    checkAccount(account);
    let array = selectedTokens[account][name];
    if (selected) {
      array.push(tokenId);
    } else {
      let index = array.indexOf(tokenId);
      array.splice(index, 1);
    }
  }

  const anotherAccount = AnotherAccount(SYSTEM_ADDRESS, selectCallback);

  const prepareTransfer = () => {
    let trader = null;
    for (const [account, tokens] of Object.entries(selectedTokens)) {
      if (account.toLowerCase() != currentAccount.toLowerCase()) {
        trader = account;
        break;
      }
    }
    if (trader === null) {
      alert("Didn't select another account");
      return;
    }
    console.log(selectedTokens);
    SELECTED_TOKENS = selectedTokens;
    checkNft(trader, selectedTokens);
    return ;
  }
  
  const getTrader = () => {
    let trader = null;
    for (const [account, tokens] of Object.entries(boardData)) {
      if (account.toLowerCase() != currentAccount.toLowerCase()) {
        trader = account;
        break;
      }
    }
    return trader;
  }

  const transferToken = async () => {
    let trader = null;
    if (Object.keys(selectedTokens).length === 0) {
      selectedTokens = SELECTED_TOKENS;
    }
    for (const [account, tokens] of Object.entries(selectedTokens)) {
      if (account.toLowerCase() != CURRENT_ACCOUNT.toLowerCase()) {
        trader = account;
        break;
      }
    }
    console.log(selectedTokens);
    let tokens = selectedTokens[Object.keys(selectedTokens).find(k => k.toLowerCase() === CURRENT_ACCOUNT)];
    const {ethereum} = window;
    const provider = new ethers.providers.Web3Provider(ethereum);
    const signer = provider.getSigner();
    console.log(selectedTokens);
    console.log(currentAccount);
    for (let tokenId of tokens.CollegeNFT) {
      let connectedContract = new ethers.Contract(
        COLLEGENFT_ADDRESS,
        CollegeNFT.abi,
        signer
      );
      console.log(`transfer from: ${CURRENT_ACCOUNT} to: ${trader} name: CollegeNFT tokenId: ${tokenId}`);
      let trans = await connectedContract["safeTransferFrom(address,address,uint256)"](CURRENT_ACCOUNT, trader, tokenId);
      await trans.wait();
    }

    for (let tokenId of tokens.UTBC) {
      let connectedContract = new ethers.Contract(
        UTBC_ADDRESS,
        CollegeNFT.abi,
        signer
      );
      console.log(`transfer from: ${CURRENT_ACCOUNT} to: ${trader} name: UTBC tokenId: ${tokenId}`);
      let trans = await connectedContract["safeTransferFrom(address,address,uint256)"](CURRENT_ACCOUNT, trader, tokenId);
      await trans.wait();
    }
  }

  const [showDashBoard, setShowDashBoard] = useState(false);
  const [boardData, setBoardData] = useState(null);
  const [showAgreement, setAgreement] = useState(false);

  return (
    <ThemeProvider theme={mdTheme}>
      <Box sx={{ display: 'flex' }}>
        <CssBaseline />
        <AppBar position="absolute" open={open}>
          <Toolbar
            sx={{
              pr: '24px', // keep right padding when drawer closed
            }}
          >
            <IconButton
              edge="start"
              color="inherit"
              aria-label="open drawer"
              onClick={toggleDrawer}
              sx={{
                marginRight: '36px',
                ...(open && { display: 'none' }),
              }}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              component="h1"
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              Ether Barter Dashboard
            </Typography>
            <IconButton color="inherit">
              <Badge badgeContent={4} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer variant="permanent" open={open}>
          <Toolbar
            sx={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'flex-end',
              px: [1],
            }}
          >
            <IconButton onClick={toggleDrawer}>
              <ChevronLeftIcon />
            </IconButton>
          </Toolbar>
          <Divider />
          <List component="nav">
            {mainListItems}
            <Divider sx={{ my: 1 }} />
          </List>
        </Drawer>
        <Box
          component="main"
          sx={{
            backgroundColor: (theme) =>
              theme.palette.mode === 'light'
                ? theme.palette.grey[100]
                : theme.palette.grey[900],
            flexGrow: 1,
            height: '100vh',
            overflow: 'auto',
          }}
        >
          <Toolbar />
          <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
            <Grid container spacing={3}>
              
            <Grid item xs={12} md={4} lg={3}>
              <Paper
                  sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: 240,
                  }}
                >
                <Account />
              </Paper>
            </Grid>
            <Grid item xs={12} md={8} lg={9}>
                <Paper
                  sx={{
                    p: 2,
                    display: 'flex',
                    flexDirection: 'column',
                    height: 240,
                  }}
                >
                  {showAgreement ? <div style={{'color': 'red'}}>Congratulations, barterers have reached an agreement !! <p></p>Please authorize the transaction by provide your allowance to EtherBarter. <p></p>Your transaction will be done in about 1 minute.</div> 
                          : showDashBoard ? <Board data={boardData} trader={getTrader()} account={currentAccount} />
                            : <Collection system_address={SYSTEM_ADDRESS} callback={selectCallback}/>}
                </Paper>
              </Grid>
            </Grid>
            <p></p>
            <div className='center'>
              <Button variant="contained" onClick={mintNft}>MINT</Button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <Button variant="contained" onClick={prepareTransfer}>CHECK</Button>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <Button variant="contained" onClick={confirmMsg}>CONFIRM</Button>
            </div>
            <p></p>
            {showAgreement || showDashBoard ? null : anotherAccount}
            <Copyright sx={{ pt: 4 }} />
          </Container>
        </Box>
      </Box>
    </ThemeProvider>
  );
}

export default App;