import * as React from 'react';
import { useState, useEffect } from "react";
import { ethers } from "ethers";
import { FormControlLabel, FormGroup, Checkbox } from '@mui/material'
import './styles/Account.css';
import BarterSystem from './utils/BarterSystem.json';

const parseCollections = (collections) => {
  const result = [];
  let index = 0;
  while (index < collections.length) {
    if (collections[index] === '') {
      break;
    }
    const name = collections[index++];
    while (index < collections.length) {
      const tokenId = collections[index++];
      if (tokenId === '<EOT>') {
        break;
      }
      result.push({name: name, tokenId: parseInt(tokenId)});
    }
  }
  return result;
}


export default function Collection(props) {
  const [currentAccount, setCurrentAccount] = useState("");
  const [collections, setCollections] = useState("");

  /*
   * Connects to Metamask wallet
   */
  const connectWallet = async () => {
    try {
      const { ethereum } = window;

      if (!ethereum) {
        alert("Get MetaMask!");
        return;
      }

      const accounts = await ethereum.request({
        method: "eth_requestAccounts",
      });

      setCurrentAccount(accounts[0]);
      const currentAccount = accounts[0];
      const provider = new ethers.providers.Web3Provider(ethereum);
      const signer = provider.getSigner();
      const systemContract = new ethers.Contract(
        props.system_address,
        BarterSystem.abi,
        signer
      );
      let allCollections = await systemContract.getAllCollectionsFrom(currentAccount);
      setCollections(parseCollections(allCollections));
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {connectWallet()});

  return (
    <React.Fragment>
      <FormGroup style={{maxHeight:'200px', maxWidth: '400px', scrollBehavior: 'smooth'}}>
      {collections.length === 0 ? 'No Collections so far' : 
        collections.map((elem) =>
          <FormControlLabel onChange={(event) => {
            props.callback(currentAccount, elem.name, elem.tokenId, event.target.checked);
          }}
            control={<Checkbox />} label={`${elem.name}, tokenId:${elem.tokenId}`}/>)
      }
      </FormGroup>
    </React.Fragment>
  );
}