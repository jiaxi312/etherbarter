// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "@openzeppelin/contracts/utils/Counters.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./Tradable.sol";

contract UTBC is ERC721, Ownable, Tradable {
    using Counters for Counters.Counter;
    Counters.Counter private _tokenIds;

    event TokenMinted(address indexed nftHolder);

    string private _name;
    string private _symbol;

    address[] private allTokens;

    constructor() ERC721("UTBigCoin", "UTBC") {
        _name = "UTBigCoin";
        _symbol = "UTBC";
        allTokens = new address[](100);
    }

    /**
     * Sets the _name and _symbol with given parameters
     */
    function setNameAndSymbol(string memory name, string memory symbol) public onlyOwner {
        _name = name;
        _symbol = symbol;
    }

    /**
     * Mints the NFT token to a given receiver with specified URI
     */
    function mintUTBC(address receiver) public returns (uint256) {
        _tokenIds.increment();

        uint256 newNtfTokenId = _tokenIds.current();
        _mint(receiver, newNtfTokenId);

        emit TokenMinted(receiver);

        allTokens[newNtfTokenId] = receiver;

        return newNtfTokenId;
    }

    function name() public view virtual override(ERC721, Tradable) returns (string memory) {
        return _name;
    }

    function symbol() public view virtual override returns (string memory) {
        return _symbol;
    }

    function getAllTokensFrom(address addr) public view override returns (uint256[] memory) {
        uint length = 0;
        for (uint i = 0; i < allTokens.length; i++) {
            if (allTokens[i] == addr) {
                length++;
            }
        }

        uint256[] memory result = new uint256[](length);
        uint256 index = 0;
        for (uint i = 0; i < allTokens.length; i++) {
            if (allTokens[i] == addr) {
                result[index++] = i;
            }
        }
        return result;
    }

    function ownerOf(uint256 tokenId) public view override(ERC721, Tradable) returns (address) {
        return ERC721.ownerOf(tokenId);
    }

    function getNumTokens(address addr) public view override returns (uint256) {
        uint length = 0;
        for (uint i = 0; i < allTokens.length; i++) {
            if (allTokens[i] == addr) {
                length++;
            }
        }
        return length;
    }

    function safeTransferFrom(address from, address to, uint256 tokenId) public override (ERC721) {
        ERC721.safeTransferFrom(from, to, tokenId);
        allTokens[tokenId] = to;
    }
}