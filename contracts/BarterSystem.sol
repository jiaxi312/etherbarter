pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Strings.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";


import "./CollegeNFT.sol";
import "./UTBC.sol";
import "./Tradable.sol";

contract BarterSystem {

    address private collegeNFTAddr;
    address private UTBCAddr;

    constructor(address _collegeNFTAddr, address _UTBCAddr) public {
        collegeNFTAddr = _collegeNFTAddr;
        UTBCAddr = _UTBCAddr;
    }

    function ownerOf(uint256 tokenId) public view returns (address) {
        return CollegeNFT(collegeNFTAddr).ownerOf(tokenId);
    }

    function getAllCollectionsFrom(address addr) public view returns (string[] memory) {
        string[] memory result = new string[](100);
        
        uint index = 0;
        result[index++] = "CollegeNFT";
        uint256[] memory allTokens = CollegeNFT(collegeNFTAddr).getAllTokensFrom(addr);
        for (uint i = 0; i < allTokens.length; i++) {
            result[index++] = Strings.toString(allTokens[i]);
        }
        result[index++] = "<EOT>";

         result[index++] = "UTBC";
        allTokens = UTBC(UTBCAddr).getAllTokensFrom(addr);
        for (uint i = 0; i < allTokens.length; i++) {
            result[index++] = Strings.toString(allTokens[i]);
        }
        result[index++] = "<EOT>";
        return result;
    }

}