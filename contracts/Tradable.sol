// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.8.0;

interface Tradable {
    /**
    * This interface represents a Tradable object, any contract that implements
    * Tradable is able to trade-in using the BarterSystem
    */

    /**
     * Returns a list of tokens owned by the given address
     */
    function getAllTokensFrom(address addr) external view returns (uint256[] memory);

    /**
     * Returns the address of the owner of the tokenId
     */
    function ownerOf(uint256 tokenId) external view returns (address);

    /**
     * Returns the name of this object
     */
    function name() external view returns (string memory);

    /**
     * Returns the total number of tokens of given address
     */
    function getNumTokens(address) external view returns (uint256);
}