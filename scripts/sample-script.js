// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const hre = require("hardhat");

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const CollegeNFT = await hre.ethers.getContractFactory("CollegeNFT");
  const collegeNft = await CollegeNFT.deploy();

  await collegeNft.deployed();

  console.log("CollegeNFT deployed to:", collegeNft.address);

  const UTBC = await hre.ethers.getContractFactory("UTBC");
  const utbc = await UTBC.deploy();

  await utbc.deployed();

  console.log("UTBC deployed to:", utbc.address);

  
  // const COLLEGENFT_ADDRESS = "0xB693c859b39cA89fA6CA3c5B3D6D291F5b4E261B";
  // const UTBC_ADDRESS = "0xF04f749E931a0bC285D86A36Fa5f49D888bdC3d1";

  const BarterSystem = await hre.ethers.getContractFactory("BarterSystem");
  const barterSystem = await BarterSystem.deploy(collegeNft.address, utbc.address);

  await barterSystem.deployed();
  console.log("BarterSystem deployed to:", barterSystem.address);
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
